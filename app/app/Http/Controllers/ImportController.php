<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImportRequest;
use App\Http\Services\ImportFromExcelService;
use App\Models\ImportedFile;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class ImportController extends BaseController
{
    /**
     * @var ImportFromExcelService
     */
    private $importFromExcelService;

    /**
     * ImportController constructor.
     * @param ImportFromExcelService $importFromExcelService
     */
    public function __construct(ImportFromExcelService $importFromExcelService)
    {
        $this->importFromExcelService = $importFromExcelService;
    }

    /**
     * @param ImportRequest $request
     * @return JsonResponse
     */
    public function invoices(ImportRequest $request)
    {
        try {
            $this->importFromExcelService->importInvoices($request->file);
            $path = $request->file('file')->getRealPath();
            $data = array_map('str_getcsv', file($path));
            ImportedFile::create([
                'user_id' => Auth::id(),
                'filename' => $request->file('file')->getRealPath(),
                'data' => json_encode($data)
            ]);
            return response()->json(['message' => trans('messages.import.success')], Response::HTTP_OK);
        } catch (Exception $e) {
            return response()->json(['message' => trans('messages.import.failed'), 'error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param ImportRequest $request
     * @return JsonResponse
     */
    public function clients(ImportRequest $request)
    {
        try {
            $this->importFromExcelService->importClients($request->file);
            $path = $request->file('file')->getRealPath();
            $data = array_map('str_getcsv', file($path));
            ImportedFile::create([
                'user_id' => Auth::id(),
                'filename' => $request->file('file')->getRealPath(),
                'data' => json_encode($data)
            ]);
            return response()->json(['message' => trans('messages.import.success')], Response::HTTP_OK);
        } catch (Exception $e) {
            return response()->json(['message' => trans('messages.import.failed'), 'error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
