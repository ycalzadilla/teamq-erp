<?php namespace App\Http\Services;

use App\Models\Imports\ClientsImport;
use App\Models\Imports\InvoicesImport;
use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;

class ImportFromExcelService
{
    /**
     * @var Excel
     */
    private $excel;

    /**
     * ImportFromExcelService constructor.
     * @param Excel $excel
     */
    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }

    public function importClients(UploadedFile $file)
    {
        Excel::import(new ClientsImport, $file);
    }

    public function importInvoices(UploadedFile $file)
    {
        Excel::import(new InvoicesImport, $file);
    }
}
