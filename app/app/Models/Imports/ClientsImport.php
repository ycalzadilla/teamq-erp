<?php

namespace App\Models\Imports;

use App\Models\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ClientsImport implements ToModel, WithValidation, SkipsOnFailure, WithHeadingRow
{
    use Importable, SkipsFailures;

    /**
     * @param array $row
     *
     * @return Client
     */
    public function model(array $row)
    {
        $address = isset($row['address']) ? explode($row['address'], ' ') : null;
        $postalCode  = isset($address[0]) && is_numeric($address[0]) ? $address[0] : "";
        $city  = isset($address[1]) ? $address[1] : "";

        $telephone = isset($row['telephone']) ? str_replace(['/', '-', ' '], ['', '', ''], $row['telephone']) : '';

        return new Client([
            'user_id' => Auth::id(),
            'internal_id' => $row['internal_id'],
            'status' => 1,
            'first_name' => $row['first_name'] ?? 'Undefined',
            'last_name' => $row['last_name'] ?? 'Undefined',
            'email' => $row['email'] ?? '',
            'telephone' => $telephone,
            'mobile' => "",
            'address' => $row['address'],
            'postal_code' => $postalCode,
            'city' => $city,
            'country' => "",
            'cellagon_id' => $row['cellagon_id'] ?? '',
        ]);
    }

    public function rules(): array
    {
        return [
            'email' => Rule::unique('clients', 'email'),
            'internal_id' => Rule::unique('clients', 'internal_id'),
        ];
    }
}
