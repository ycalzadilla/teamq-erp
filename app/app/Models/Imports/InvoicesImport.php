<?php

namespace App\Models\Imports;

use App\Models\Client;
use App\Models\Invoice;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class InvoicesImport implements ToModel, WithHeadingRow
{
    use Importable;

    /**
     * @param array $row
     *
     * @return Invoice
     */
    public function model(array $row)
    {
        $client = Client::where('internal_id', "{$row['Kd-Nr.']}")->get()->first();
        $product = Product::where('description', 'like', "%{$row['Matchcode']}%")->get()->first();

        return new Invoice([
            'user_id' => Auth::id(),
            'client_id' => $client ? $client->id : null,
            'product_id' => $product ? $product->id : null,
            'internal_nr' => $client ? $client->internal_id : null,
            'invoice_nr' => $row['Belegnr.'] ?? 'Undefined',
            'date' => $row['Datum'] ?? 'Undefined',
            'amount' => $row['Gesamt'] ?? 0.00,
            'discount' => 0.00,
            'tax' => 0.00,
            'quantity' => 0,
        ]);
    }
}
