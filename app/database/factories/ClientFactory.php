<?php

/** @var Factory $factory */

use App\Models\Client;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Client::class, function (Faker $faker) {

    return [
        'user_id' => User::all()->random()->id,
        'internal_id' => $faker->randomNumber(),
        'status' => 1,
        'first_name' => $faker->firstName(),
        'last_name' => $faker->firstName(),
        'email' => $faker->unique()->safeEmail,
        'telephone' => $faker->unique()->phoneNumber,
        'mobile' => $faker->unique()->phoneNumber,
        'address' => $faker->unique()->address,
        'postal_code' => $faker->unique()->postcode,
        'city' => $faker->unique()->city,
        'country' => $faker->unique()->country,
        'cellagon_id' => $faker->randomNumber(),

    ];
});
