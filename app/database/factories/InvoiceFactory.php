<?php

/** @var Factory $factory */

use App\Models\Client;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Invoice::class, function (Faker $faker) {

    $amount = $faker->numberBetween(100, 50000);
    $percent = $faker->numberBetween(0, 50);
    $discount = $percent ? ($amount * $percent) / 100 : 0;
    $tax = round($amount * 0.12, 2);

    return [
        'user_id' => User::all()->random()->id,
        'client_id' => Client::all()->random()->id,
        'product_id' => Product::all()->random()->id,
        'internal_nr' => function() {
            return Client::select(['internal_id'])->orderByRaw('RAND()')->first()->internal_id;
        },
        'invoice_nr' => $faker->randomNumber(),
        'date' => $faker->dateTimeBetween('-3 years', 'now'),
        'amount' => $amount,
        'discount' => $discount,
        'tax' => $tax,
        'quantity' => $faker->numberBetween(2, 50),
    ];
});
