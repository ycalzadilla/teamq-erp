<?php

/** @var Factory $factory */

use App\Models\Product;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'user_id' => User::all()->random()->id,
        'status' => 1,
        'description' => $faker->unique()->name,
    ];
});
