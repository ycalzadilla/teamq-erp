<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('internal_nr')->nullable()->comment('Client Internal Nr');
            $table->integer('invoice_nr')->nullable()->comment('Beleg Nr');
            $table->dateTime('date')->nullable();
            $table->float('amount',10,2)->nullable();
            $table->float('discount',10,2)->nullable();
            $table->float('tax',10,2)->nullable();
            $table->integer('quantity')->unsigned();
            $table->softDeletes();
            $table->timestamps();

        });

        DB::statement('set foreign_key_checks=0');
        Schema::table('invoices', function (Blueprint $table) {
            $table->foreign('user_id', 'fk_invoices_user')->references('id')->on('users');
            $table->foreign('client_id', 'fk_invoices_client')->references('id')->on('clients');
            $table->foreign('product_id', 'fk_invoices_product')->references('id')->on('products');
        });
        DB::statement('set foreign_key_checks=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
