<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('status',2)->nullable();
            $table->string('description')->nullable();
            $table->softDeletes();
            $table->timestamps();

        });

        DB::statement('set foreign_key_checks=0');
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('user_id', 'fk_products_user')->references('id')->on('users');
        });
        DB::statement('set foreign_key_checks=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('set foreign_key_checks=0');
        Schema::dropIfExists('products');
        DB::statement('set foreign_key_checks=1');
    }
}
