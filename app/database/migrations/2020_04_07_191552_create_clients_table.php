<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('internal_id')->nullable();
            $table->string('status',2)->nullable();
            $table->string('first_name',80);
            $table->string('last_name',80);
            $table->string('email',80)->nullable();
            $table->string('telephone',30)->nullable();
            $table->string('mobile',30)->nullable();
            $table->string('address')->nullable();
            $table->string('postal_code',20)->nullable();
            $table->string('city',100)->nullable();
            $table->string('country',100)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        DB::statement('set foreign_key_checks=0');
        Schema::table('clients', function (Blueprint $table) {
            $table->foreign('user_id', 'fk_clients_user')->references('id')->on('users');
        });
        DB::statement('set foreign_key_checks=1');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('set foreign_key_checks=0');
        Schema::dropIfExists('clients');
        DB::statement('set foreign_key_checks=1');
    }
}
